{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Main where

import ClassyPrelude.Yesod hiding (setMessage, intersect, union, intercalate)
import Control.Monad.Logger
import Data.Text (Text)
import Data.Aeson
import Data.Time (readTime)
import Data.List (intercalate, intersect, nub, union)
import Data.Maybe (fromJust)
import Database.Persist.Sqlite
import Database.Persist.TH
import Yesod hiding (setMessage)
import Yesod.Core hiding (setMessage)
import Yesod.Core.Json
import Network.Api.TextLocal
import Data.Time.Clock (UTCTime)
import qualified Data.Text as T
import Network.Api.Types
import Network.HTTP.Simple
import qualified Data.ByteString.Char8 as BC
import qualified Data.Text.Encoding as T

share
  [ mkPersist
      sqlSettings
      { mpsGeneric = False
      }
  , mkMigrate "migrateAll"
  ]
  [persistLowerCase|
Person json
    phone Text
    name Text Maybe
    answered [Text]
    UniquePhone phone
    deriving Show

Log json
    person PersonId
    log Text
    timestamp UTCTime default=CURRENT_TIMESTAMP
    deriving Show
|]

data App =
  App SqlBackend

mkYesod
  "App"
  [parseRoutes|
/ HomeR GET
/inbox InboxR POST
/riddle-1 Riddle1R POST
|]

answers :: [Text]
answers = ["FIRST", "SECOND", "THIRD", "FOURTH", "FIFTH"]

instance Yesod App where
  approot = ApprootStatic "http://localhost:3005"

instance RenderMessage App FormMessage where
  renderMessage _ _ = defaultFormMessage

instance YesodPersist App where
  type YesodPersistBackend App = SqlBackend
  runDB f = do
    App conn <- getYesod
    runSqlConn f conn

getHomeR :: Handler Html
getHomeR = do
  defaultLayout $
    do toWidget
         [whamlet|
            <p>Home page
         |]

validateAnswers :: Maybe Text -> [Text] -> [Text]
validateAnswers Nothing ys = nub ys
validateAnswers (Just xs) ys = nub $ (xs' `union` ys) `intersect` answers
  where
    (xs' :: [Text]) = T.split (== ' ') xs

hasAnswers :: [Text] -> Bool
hasAnswers xs = length xs > 0

sendResponseSMS
  :: (MonadLogger m, MonadIO m)
  => Bool -- ^ Has any valid answer so far
  -> [Text] -- ^ His correct answers so far
  -> ByteString -- ^ His phone number (should start from 91)
  -> Credential
  -> m (Either JSONException TLResponse)
sendResponseSMS valid ans pno cred = do
  case (length ans) of
    5 -> do
      resp <- liftIO $ runSettings SendSMS (setMessage "Congrats on solving the riddle. You deserve a worthy prize from us and are one step closer to getting one. Stay tuned for the next riddle. Until then, CHOW!" mySettings)
      let logMsg = T.pack $ show resp
      logInfoN logMsg
      return resp
    _ -> do
      resp <- liftIO $ runSettings SendSMS (setMessage sucMsg mySettings)
      let logMsg = T.pack $ show resp
      logInfoN $ decodeUtf8 sucMsg
      logInfoN logMsg
      return resp
  where
    ans' :: ByteString
    ans' = T.encodeUtf8 $ (T.intercalate "-" ans)
    mySettings =
      setSender "XXXXXX" $ setDestinationNumber [pno] $ setAuth cred defaultSMSSettings
    sucMsg =
      "Thanks for trying. You've got " <>
      (BC.pack $ show (length ans)) <>
      " words right - " <>
      (if ans' == "" then "Hah" else ans') <>
      ". Waiting for you at the other side - www.xxxxxxxxxxxx.com"

postInboxR :: Handler Html
postInboxR = do
  (rcvd :: Maybe Text) <- lookupPostParam "rcvd"
  sender <- lookupPostParam "sender"
  answer <- (fmap . fmap) toUpper $ lookupPostParam "comments"
  logInfoN $ T.pack $ show sender
  _ <-
    runDB $
    do (person :: Maybe (Entity Person)) <-
         getBy $ UniquePhone (fromJust sender)
       pid <-
         case person of
           Nothing -> insert $ Person (fromJust sender) Nothing []
           Just pers -> return $ entityKey pers
       let answers'' :: [Text] =
             if (isJust person)
               then validateAnswers
                      answer
                      (personAnswered $ entityVal (fromJust person))
               else validateAnswers answer []
           hasAns :: Bool = hasAnswers answers''
       insert_ $
         Log
           pid
           (fromJust answer)
           (readTime defaultTimeLocale rcvdFormat $ unpack $ fromJust rcvd)
       updateWhere [PersonId ==. pid] [PersonAnswered =. answers'']
       lift $ sendResponseSMS hasAns answers'' (T.encodeUtf8 (fromJust sender)) txtLclCreds
  defaultLayout [whamlet|hello world|]
  where
    txtLclCreds :: Credential
    txtLclCreds =
      createUserHash
        "tech.admin@xxxxxxxxxxxx.com"
        "asfjgnsdjn~oO"
    settings :: SMSSettings
    settings = setAuth txtLclCreds $ setTest True defaultSMSSettings
    rcvdFormat :: String
    rcvdFormat = "%Y-%m-%d %H:%M:%S"

data Answer = Answer
  { _1 :: Text
  , _2 :: Text
  , _3 :: Text
  , _4 :: Text
  , _5 :: Text
  , email :: Text
  } deriving (Show)

instance FromJSON Answer where
  parseJSON (Object o) = Answer
    <$> o .: "_1"
    <*> o .: "_2"
    <*> o .: "_3"
    <*> o .: "_4"
    <*> o .: "_5"
    <*> o .: "email"
  parseJSON _ = mzero

instance ToJSON Answer where
  toJSON (Answer _1 _2 _3 _4 _5 email) = (object . filter notNull)
      [ "_1" .= _1
      , "_2" .= _2
      , "_3" .= _3
      , "_4" .= _4
      , "_5" .= _5
      , "email" .= email
      ]
    where
      notNull (_, Null) = False
      notNull _ = True

postRiddle1R :: Handler Value
postRiddle1R = do
  addHeader "Access-Control-Allow-Origin" "*"
  addHeader "Access-Control-Allow-Methods" "PUT, OPTIONS, POST"
  answer <- requireJsonBody :: Handler Answer
  let answer' = fmap toUpper $ _1 answer : _2 answer : _3 answer : _4 answer : _5 answer : []
  let email' = email answer
  logInfoN $ (T.pack . show) answer'
  time <- liftIO $ getCurrentTime
  a <-
    runDB $
    do (person :: Maybe (Entity Person)) <-
         getBy $ UniquePhone email' -- phone field is reused for email
       pid <-
         case person of
           Nothing -> insert $ Person email' Nothing []
           Just pers -> return $ entityKey pers
       let answers'' :: [Text] =
             if (isJust person)
                then validateAnswers
                     (Just (unwords answer'))
                     (personAnswered $ entityVal (fromJust person))
                else validateAnswers
                     (Just (unwords answer'))
                     []
           hasAns :: Bool = hasAnswers answers''
       logInfoN $ (T.pack . show) answers''
       insert_ $
         Log
           pid
           (unwords answer')
           time
       updateWhere [PersonId ==. pid] [PersonAnswered =. answers'']
       return answers''
  returnJson $ array a

main :: IO ()
main =
  runStdoutLoggingT $
  withSqliteConn "smsrobot.sqlite3" $
  \conn ->
     liftIO $
     do runSqlConn (runMigration migrateAll) conn
        warp 3005 $ App conn
